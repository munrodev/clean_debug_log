<?php
namespace Doug\CodeTest\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LogCommand extends Command
{
    /**
     * Name argument
     */
    const NAME_ARGUMENT = 'name';
    /**
     * Allow option
     */
    const ALLOW_ANONYMOUS = 'allow-anonymous';
    /**
     * Anonymous name
     */
    const ANONYMOUS_NAME = 'Anonymous';

    private $firstCacheEntry = true;

    private $firstDebugEntry = true;
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('debug:clean')
            ->setDescription('Clean the debug log cache');

        parent::configure();
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting .......');
        $debugFile = file_get_contents('/var/www/html/ECC2/var/log/debug.log');

        $logArray = $lines = preg_split( '/\r\n|\r|\n/', $debugFile );

        foreach($logArray as $logEntry)
        {
            if(!strpos($logEntry, 'cache_invalidate')){
                $this->writeToDebugEntries($logEntry);
            }else{
                $this->writeToCacheLog($logEntry);
            }
        }

        //file_put_contents('/var/www/html/ECC2/var/log/debug.log', '');
    }

    private function writeToDebugEntries($cacheEntry)
    {
        if($this->firstDebugEntry){
            file_put_contents('/var/www/html/ECC2/var/log/debug_entry.log', $cacheEntry);
            $this->firstDebugEntry = false;
        }else{
            file_put_contents('/var/www/html/ECC2/var/log/debug_entry.log', $cacheEntry, FILE_APPEND);
        }
    }

    private function writeToCacheLog($cacheEntry)
    {
        if($this->firstCacheEntry){
            file_put_contents('/var/www/html/ECC2/var/log/debug_cache.log', $cacheEntry);
            $this->firstCacheEntry = false;
        }else{
            file_put_contents('/var/www/html/ECC2/var/log/debug_cache.log', $cacheEntry, FILE_APPEND);
        }
    }
}